<?php

namespace App\Bitm\Seip135212\BirthDate;

use App\Bitm\Seip135212\Message\Message;
use App\Bitm\Seip135212\Utility\Utility;
use PDO;

class BirthDate
{
    public $id = "";
    public $BirthDate = "";
    public $created = "";
    public $modified = "";
    public $created_by = "";
    public $modified_by = "";
    public $deleted_at = "";
    public $servername = "localhost";
    public $databasename = "AtomicProject";
    public $password = "";
    public $username = "root";
    public $conn;


    public function __construct()
    {

        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->databasename", $this->username, $this->password);
            // set the PDO error mode to exception
//            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          //  echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

    }

    public function setdata($data = "")
    {
        if (array_key_exists('BirthDate', $data) and !empty($data)) {
            $this->BirthDate = $data['BirthDate'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function Index()
    {
        $sqlquery="SELECT * FROM `birthdate`";
        $res=$this->conn->query($sqlquery);
        $alldata=$res->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }



    public function Created()
    {
        return "I'm created form<br>";
    }

    public function Store()
    {
        $query = "INSERT INTO `birthdate` (`BirthDate`) VALUES (:BirthDate)";
        $result = $this->conn->prepare($query);
        $result->execute(array(':BirthDate' => $this->title));
        if($result)
        {Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> data has been stored.
</div>");
            Utility::redirect("index.php");
        }
        else{
            Message::message("<div class=\"alert alert-danger\">
  <strong>wrong!</strong> data has not been stored.
</div>");
            Utility::redirect("index.php");

        }

    }

    public function Edit()
    {
        return "I'm EDITING data<br>";
    }

    public function view()
    {
        $sqlquery="SELECT * FROM `birthdate` WHERE `id`=:id";
        $res=$this->conn->prepare($sqlquery);
        $res->execute(array(':id'=>$this->id));
        $singledata=$res->fetch(PDO::FETCH_OBJ);
        return $singledata;
    }
    public function Delete()
    {
        return "I'm Deleting data<br>";
    }

}
?>