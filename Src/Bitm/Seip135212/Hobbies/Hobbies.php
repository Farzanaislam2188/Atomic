<?php
namespace App\Bitm\Seip135212\Hobbies;
use PDO;
use App\Bitm\Seip135212\Utility\Utility;
use App\Bitm\Seip135212\Message\Message;
class Hobbies
{
    public $id = "";
    public $name = "";
    public $hobby_list = "";
    public $modified = "";
    public $password = "";
    public $username = "root";
    public $servername = "localhost";
    public $database = "atomicproject";
    public $deleted_at = '';

    public function __construct()
    {

        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->database", $this->username, $this->password);
            // set the PDO error mode to exception
            // $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

    }

    public function Setdata($data)
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('hobby_list', $data) and !empty($data)) {
            $this->hobby_list = $data['hobby_list'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function Index()
    {
        $query = "SELECT * FROM `hobby`WHERE `deleted_at`IS NULL ";
        $smt = $this->conn->query($query);
        $alldata = $smt->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }

    public function Store()
    {
        $query = "INSERT INTO `hobby` ( `name`, `hobby_list`) VALUES ( :name, :hobby);";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':name' => $this->name, ':hobby' => $this->hobby_list));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> UPDATED SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }
    }

    public function view()
    {
        $query = "SELECT * FROM `hobby` WHERE `id` = :id";
        $smt = $this->conn->prepare($query);
        $smt->execute(array(':id' => $this->id));
        $rt = $smt->fetch(PDO::FETCH_OBJ);
        return $rt;
    }


    public function Update()
    {
        $query = "UPDATE `hobby` SET `name` = :eml, `hobby_list` = :pow WHERE `hobby`.`id` = :id";
        $smt = $this->conn->prepare($query);
        $result = $smt->execute(array(':eml' => $this->name, ':pow' => $this->hobby_list, ':id' => $this->id));

        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> UPDATED SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }
    }

    public function Delete()
    {
        $query = "DELETE FROM `hobby` WHERE `hobby`.`id` = :id";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':id' => $this->id));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> deleted SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }

    }

    public function Trash()
    {
        $this->deleted_at = date("Y-m-d");
        $query = " UPDATE `hobby` SET `deleted_at` = :deleted_at WHERE `hobby`.`id` = :id";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':deleted_at' => $this->deleted_at, ':id' => $this->id));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> TRASHED SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }
    }

    public function Trashed()
    {
        $query = "SELECT * FROM `hobby`WHERE `deleted_at`IS NOT NULL ";
        $smt = $this->conn->query($query);
        $alldata = $smt->fetchAll(PDO::FETCH_OBJ);
        return $alldata;

    }

    public function Restore()
    {
        $this->deleted_at = NULL;
        $query = " UPDATE `hobby` SET `deleted_at` = :deleted_at WHERE `hobby`.`id` = :id";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':deleted_at' => $this->deleted_at, ':id' => $this->id));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> restore SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }

    }
}


