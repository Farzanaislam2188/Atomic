<?php

namespace App\Bitm\Seip135212\Email;
session_start();

use App\Bitm\Seip135212\Message\Message;
use App\Bitm\Seip135212\Utility\Utility;
use PDO;

class Email
{
    public $id = "";
    public $email_id = "";
    public $servername = "localhost";
    public $username = "root";
    public $database = "AtomicProject";
    public $password = "";
    public $conn;

    public function __construct()
    {

        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->database", $this->username, $this->password);
            // set the PDO error mode to exception
            // $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

    }

    public function Setdata($data)
    {  if (array_key_exists('id', $data) and !empty($data)) {
        $this->id = $data['id'];

    }
        if (array_key_exists('email_id', $data) and !empty($data)) {

            $this->email_id = $data['email_id'];
        }

        if (array_key_exists('password', $data) and !empty($data)) {
            $this->password = $data['password'];

        }
        return $this;
    }


    public function Index()
    {
        $query = "SELECT * FROM `email`";
        $smt = $this->conn->query($query);
        $alldata = $smt->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }


    public function Store()
    {
        $query = "INSERT INTO `email` ( `email_id`, `password`) VALUES (:email, :pwd)";
        $smt = $this->conn->prepare($query);
        $result = $smt->execute(array(':email' => $this->email_id, ':pwd' => $this->password));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Indicates a successful or positive action.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }

    }

    public function Update()
    {
        $query = "UPDATE `email` SET `email_id` = :eml, `password` = :pow WHERE `email`.`id` = :id";
        $smt = $this->conn->prepare($query);
$result=$smt->execute(array(':eml' => $this->email_id, ':pow' => $this->password ,':id' => $this->id));

        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> UPDATED SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }
    }



    public function view()
    {
        $query = "SELECT * FROM `email` WHERE `id` = :id";
        $smt = $this->conn->prepare($query);
        $smt->execute(array(':id' => $this->id));
        $rt = $smt->fetch(PDO::FETCH_OBJ);
        return $rt;


    }
    public function Delete()
    {
        $query = "DELETE FROM `email` WHERE `email`.`id` = :id";
        $smt = $this->conn->prepare($query);
       $result= $smt->execute(array(':id' => $this->id));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> deleted SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }


    }



}

?>