<?php

namespace App\Bitm\Seip135212\Mobile;

use App\Bitm\Seip135212\Message\Message;
use App\Bitm\Seip135212\Utility\Utility;

use PDO;

class Mobile
{
    public $id = "";
    public $title = "";
    public $mobile_model = "";
    public $servername = "localhost";
    public $databasename = "atomicproject";
    public $username = "root";
    public $password = "";
    public $conn;
    public $deleted_at = "";

    public function __construct()
    {

        try {
            $this->conn = new PDO("mysql:host=$this->servername;dbname=$this->databasename", $this->username, $this->password);
            // set the PDO error mode to exception

            echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }


    }

    public function setdata($data = "")
    {
        if (array_key_exists('title', $data) and !empty($data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('mobile_model', $data) and !empty($data)) {
            $this->mobile_model = $data['mobile_model'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function Store()
    {
        $query = "INSERT INTO `mobile` (`title`, `mobile_model`) VALUES (:sam, :small)";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':sam' => $this->title, ':small' => $this->mobile_model));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> STORED SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }
    }

    public function Index()
    {
        $sqlquery = "SELECT * FROM `mobile` WHERE `deleted_at` IS  NULL ";
        $res = $this->conn->query($sqlquery);
        $alldata = $res->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }

    public function view()
    {
        $sqlquery = "SELECT * FROM `mobile` WHERE `id`=:id";
        $res = $this->conn->prepare($sqlquery);
        $res->execute((array(':id' => $this->id)));
        $alldata = $res->fetch(PDO::FETCH_OBJ);
        return $alldata;


    }

    public function update()
    {
        $query = "UPDATE `mobile` SET `title` = :title, `mobile_model` = :mobile WHERE `mobile`.`id` = :id";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':title' => $this->title, ':mobile' => $this->mobile_model, ':id' => $this->id));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> UPDATED SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }

    }

    public function Delete()
    {
        $query = "DELETE FROM `mobile` WHERE `mobile`.`id` = :id";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':id' => $this->id));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> deleted SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }

    }

    public function Trash()
    {
        $this->deleted_at = date("Y-m-d");
        $query = " UPDATE `mobile` SET `deleted_at` = :deleted_at WHERE `mobile`.`id` = :id";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':deleted_at' => $this->deleted_at, ':id' => $this->id));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> TRASHED SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");


        }
    }

    public function Trashed()
    {
        $sqlquery = "SELECT * FROM `mobile` WHERE `deleted_at` IS NOT NULL ";
        $res = $this->conn->query($sqlquery);
        $alldata = $res->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }

    public function Restore()
    {
        $this->deleted_at = NULL;
        $query = " UPDATE `mobile` SET `deleted_at` = :deleted_at WHERE `mobile`.`id` = :id";
        $res = $this->conn->prepare($query);
        $result = $res->execute(array(':deleted_at' => $this->deleted_at, ':id' => $this->id));
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> RESTORED SUCCESSFULLY.
</div>
");
            Utility::redirect("index.php");

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>warning!</strong> Indicates a successful or ngative action.
</div>
");
            Utility::redirect("index.php");

        }

    }
}