<?php
include_once("../../../vendor/autoload.php");
use \App\Bitm\Seip135212\Email\Email;
use App\Bitm\Seip135212\Utility\Utility;
use App\Bitm\Seip135212\Message\Message;

$ob = new Email();
//$result=$ob->Setdata($_POST);
$result = $ob->Index();
//Utility::d($result)
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div id="message">
        <?php
        if (array_key_exists('var', $_SESSION) and !empty($_SESSION['var'])) {
            echo Message::message();
        }
        ?>
    </div>
    <h2>Bordered Table</h2>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>EMAIL</th>
            <th>PASSWORD</th>
            <th>ACTION</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl = 0;
        foreach ($result as $res) {
            $sl++
            ?>
            <tr>
                <td><?php echo $sl ?></td>
                <td><?php echo $res->id ?></td>
                <td><?php echo $res->email_id ?></td>
                <td><?php echo $res->password ?></td>
                <td><a href="view.php?id=<?php echo $res->id ?>" class="btn btn-success" role="button">VIEW
                    </a>
                    <a href="edit.php?id=<?php echo $res->id ?>" class="btn btn-primary" role="button">EDIT
                    </a>
                    <form action="delete.php" method="post">
                        <input type="hidden" name="id" value="<?php echo $res->id ?>">
                            <input type="submit" value="DELETE">

                    </form>
                </td>
            </tr>
        <?php } ?>

        </tbody>
    </table>
</div>
<script>
    $("#message").show().delay(5000).fadeOut();
</script>

</body>
</html>


