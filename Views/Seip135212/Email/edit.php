<?php
include_once ("../../../vendor/autoload.php");
use \App\Bitm\Seip135212\Email\Email;
use App\Bitm\Seip135212\Utility\Utility;
use App\Bitm\Seip135212\Message\Message;
$ob= new Email();
$result=$ob->Setdata($_GET)->view();
//$result->Store();
//Utility::dd($result)
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>edit form</h2>
    <form action="update.php"method="post">

        <div class="form-group">
            <input type="hidden"name="id"value="<?php echo $result->id ?>"
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" placeholder="Enter email" name="email_id" value="<?php echo $result->email_id ?>">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" placeholder="Enter password"name="password"value="<?php echo $result->password?>">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="submit" class="btn btn-default">update</button>
    </form>
</div>

</body>
</html>

