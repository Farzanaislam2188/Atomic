<?php
include_once ("../../../vendor/autoload.php");
use App\Bitm\Seip135212\BirthDate\BirthDate;
use  App\Bitm\Seip135212\Utility\Utility;
$ob=new BirthDate();
$result=$ob->setdata($_GET)->view();
//Utility::dd($result);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Basic List Group</h2>
    <ul class="list-group">

        <li class="list-group-item">ID:<?php echo $result->id ?></li>
        <li class="list-group-item">BIRTHDATE<?php echo $result->BirthDate ?></li>
    </ul>
</div>

</body>
</html>


