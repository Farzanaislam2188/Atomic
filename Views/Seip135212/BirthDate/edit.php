<?php
include_once ("../../../vendor/autoload.php");
use App\Bitm\Seip135212\BirthDate\BirthDate;
use  App\Bitm\Seip135212\Utility\Utility;
$ob=new BirthDate();
$result=$ob->setdata($_GET)->view();
//Utility::dd($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>BIRTHDATE FORM</h2>
    <form method="post"action="store.php">
        <div class="form-group">
            <label for="BIRTH">BirthDate</label>
            <input type="date" class="form-control" id="BIRTH" placeholder="Enter your birthdate"
                   name="BirthDate"value="<?php echo $result->BirthDate ?> ">
        </div>
        <button type="submit" class="btn btn-default">update</button>
    </form>
</div>

</body>
</html>
