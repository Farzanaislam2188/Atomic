<?php
session_start();
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "basapractice" . DIRECTORY_SEPARATOR . "AtomicProject" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");
use App\Bitm\Seip135212\BirthDate\BirthDate;

$ob = new BirthDate();
$result = $ob->Index();

//\App\Bitm\Seip135212\Utility\Utility::dd($result);

?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<body>
<div class="container">
    <div id="message">
        <?php
        if (array_key_exists('var', $_SESSION) and !empty($_SESSION['var'])) {
            echo \App\Bitm\Seip135212\Message\Message::message();
        }
        ?>
    </div>
    <a href="create.php" class="btn btn-primary" role="button">GO TO CREATE PAGE</a>
    <h2>Bordered Table</h2>
    <table class="table table-bordered">
        <thead>
        <tr>

            <th>SL</th>
            <th>id</th>
            <th>birthdate</th>
            <th>action</th>
        </tr>
        </thead>
        <tbody>

        <?php $sl = 0;
        foreach ($result as $res) {
            $sl++ ?>
            <tr>
                <td><?php echo $sl ?></td>
                <td><?php echo $res->id ?></td>
                <td><?php echo $res->BirthDate ?></td>
                <td><a href="view.php?id=<?php echo $res->id?>" class="btn btn-primary" role="button">VIEW</a>
                    <a href="edit.php?id=<?php echo $res->id?>" class="btn btn-success" role="button">Edit</a>
                    <a href="delete.php" class="btn btn-danger" role="button">delete</a></td>


            </tr>
        <?php } ?>

        </tbody>
    </table>
</div>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>

</body>
</html>
