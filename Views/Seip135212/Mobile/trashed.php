<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\Seip135212\Mobile\Mobile;
use App\Bitm\Seip135212\Utility\Utility;
$ob=new Mobile();
$result=$ob->setdata($_POST)->Trashed();
//Utility::dd($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>LIST OF all trashed data</h2>


    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>TITLE</th>
            <th>MOBILE MODEL</th>
            <th>ACTION</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach ($result as $rs){
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl ?></td>
                <td><?php echo $rs->id ?></td>
                <td><?php echo $rs->title ?> </td>
                <td><?php echo $rs->mobile_model ?> </td>
                <td>
                    <a href="delete.php?id=<?php echo $rs->id ?>" class="btn btn-danger" role="button">delete</a>
               <a href="restore.php?id=<?php echo $rs->id ?>" class="btn btn-info" role="button">RESTORE</a></td>
            </tr>
        <?php } ?>

        </tbody>
    </table>
</div>

</body>
</html>



