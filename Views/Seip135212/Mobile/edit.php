<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\Seip135212\Mobile\Mobile;
use App\Bitm\Seip135212\Utility\Utility;
$ob=new Mobile();
$result=$ob->setdata($_GET)->view();
//Utility::dd($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Mobile form</h2>
    <form action="update.php" method="post">
        <div class="form-group">
            <input type="hidden"name="id"value="<?php echo $result->id ?>"
            <label for="mobile">Mobile Name:</label>
            <input type="text" class="form-control" id="mobile" placeholder="Enter your mobile name"name="title"
                   value="<?php echo $result->title?> ">
        </div>
        <div class="form-group">
            <label for="category">mobile model:</label>
            <input type="text" class="form-control" id="category" placeholder="Enter your category"name="mobile_model"
                   value="<?php echo $result->mobile_model ?>">
        </div>

        <button type="submit" class="btn btn-default">update</button>
    </form>
</div>

</body>
</html>