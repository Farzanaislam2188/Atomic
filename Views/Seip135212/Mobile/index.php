<?php
session_start();
include_once ($_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR."basapractice".DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\Bitm\Seip135212\Mobile\Mobile;
$ob=new Mobile();
$result=$ob->Index();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div id="message">
        <?php
        if (array_key_exists('var', $_SESSION) and !empty($_SESSION['var'])) {
            echo \App\Bitm\Seip135212\Message\Message::message();
        }
        ?>
    <h2>LIST OF MOBILE</h2>
    <div><a href="create.php?id=<?php echo $result->id ?>" class="btn btn-info" role="button">create again</a>
        <a href="trashed.php?id=<?php echo $result->id ?>" class="btn btn-info" role="button">TRASHed data</a></td></div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>TITLE</th>
            <th>MOBILE MODEL</th>
            <th>ACTION</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach ($result as $rs){
            $sl++;
        ?>
        <tr>
            <td><?php echo $sl ?></td>
            <td><?php echo $rs->id ?></td>
            <td><?php echo $rs->title ?> </td>
            <td><?php echo $rs->mobile_model ?> </td>
            <td><a href="view.php?id=<?php echo $rs->id ?> " class="btn btn-info" role="button">VIEW</a>
            <a href="edit.php?id=<?php echo $rs->id ?>" class="btn btn-info" role="button">EDIT</a>
            <a href="delete.php?id=<?php echo $rs->id ?>" class="btn btn-info" role="button">DELETE</a>
                <a href="trash.php?id=<?php echo $rs->id ?>" class="btn btn-info" role="button">TRASH</a></td>
        </tr>
<?php } ?>

        </tbody>
    </table>
</div>

</body>
</html>


