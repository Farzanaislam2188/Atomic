<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Form control: checkbox</h2>


    <form action="store.php" method="post">
        <div class="form-group">
            <label for="name"> Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter your name" name="name">
        </div>
        <label for="hobby"> your hobby:</label>
        <div class="checkbox">
            <label><input type="checkbox" name ="hobby_list[]" value="cricket">cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox"name ="hobby_list[]" value="football">football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox"name ="hobby_list[]" value="tabletanise">tabletanise</label>
        </div>
        <div class="checkbox ">
            <label><input type="checkbox"name ="hobby_list[]" value="hockey" >hockey</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox"name ="hobby_list[]" value="golf">golf</label>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>

