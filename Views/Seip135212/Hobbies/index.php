<?php
session_start();
include_once ($_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR."basapractice".DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\Bitm\Seip135212\Hobbies\Hobbies;
use App\Bitm\Seip135212\Utility\Utility;
//Utility::dd($_POST);
$ob=new Hobbies();
$result=$ob->Index();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div id="message">
        <?php
        if (array_key_exists('var', $_SESSION) and !empty($_SESSION['var'])) {
            echo \App\Bitm\Seip135212\Message\Message::message();
        }
        ?>
        </div>
    <h2>LIST OF hobbies</h2>
    <div><a href="create.php" class="btn btn-info" role="button">CREATE AGAIN</a>
        <a href="trashed.php" class="btn btn-info" role="button">TRASHED DATA</a>
        <a href="pdf.php" class="btn btn-info" role="button">PDF DATA DOWNLOADED</a>
        <a href="xl.php" class="btn btn-info" role="button">excel DATA DOWNLOADED</a>
        <a href="mail.php" class="btn btn-info" role="button">mail send</a></div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>NAME</th>
            <th>HOBBY LIST</th>
            <th>ACTION</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach ($result as $rs){
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl ?></td>
                <td><?php echo $rs->id ?></td>
                <td><?php echo $rs->name?> </td>
                <td><?php echo $rs->hobby_list ?> </td>
                <td><a href="view.php?id=<?php echo $rs->id ?> " class="btn btn-info" role="button">VIEW</a>
                    <a href="edit.php?id=<?php echo $rs->id ?>" class="btn btn-info" role="button">EDIT</a>
                    <a href="delete.php?id=<?php echo $rs->id ?>" class="btn btn-info" role="button">DELETE</a>
                    <a href="trash.php?id=<?php echo $rs->id ?>" class="btn btn-info" role="button">TRASH</a></td>
            </tr>
        <?php } ?>

        </tbody>
    </table>


    </div>
<script>
    $("#message").show().delay(3000).fadeOut();




</script>
</body>

</html>


