<?php
include_once ($_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR."basapractice".DIRECTORY_SEPARATOR."AtomicProject".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
use App\Bitm\Seip135212\Hobbies\Hobbies;
use App\Bitm\Seip135212\Utility\Utility;

$hobby = new Hobbies();
$hobby->Setdata($_GET);
$singleHobby = $hobby->view();

$hobbyInString = $singleHobby->hobby_list;
$hobbyInArray = explode(",", $hobbyInString);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Form control: checkbox</h2>


    <form action="update.php" method="post">
        <div class="form-group">
            <input type="hidden" name="id" value="<?php echo $singleHobby->id ?>">
            <label for="name"> Name:</label>
            <input type="text" class="form-control" id="name" placeholder="Enter your name" name="name"
                   value="<?php echo $singleHobby->name ?>">
        </div>
        <label for="hobby"> your hobby:</label>
        <div class="checkbox">
            <label><input type="checkbox" value="cricket"name ="hobby_list[]" <?php if (in_array("cricket", $hobbyInArray)) {
                    echo "checked";
                } ?>>cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox"value="football"name ="hobby_list[]"<?php if (in_array("football", $hobbyInArray)) {
                    echo "checked";
                } ?>>football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox"value="tabletanise"name ="hobby_list[]"<?php if (in_array("tabletanise", $hobbyInArray)) {
                    echo "checked";
                } ?>>tabletanise</label>
        </div>
        <div class="checkbox ">
            <label><input type="checkbox"value="hockey" name ="hobby_list[]"<?php if (in_array("hockey", $hobbyInArray)) {
                    echo "checked";
                } ?>>hockey</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox"value="golf"name ="hobby_list[]"<?php if (in_array("golf", $hobbyInArray)) {
                    echo "checked";
                } ?>>golf</label>
        </div>
        <button type="submit" class="btn btn-default">UPDATE</button>
    </form>
</div>

</body>
</html>

